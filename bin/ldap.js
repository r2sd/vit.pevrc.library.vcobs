var config = require('../config.js');
var ActiveDirectory = require('activedirectory');
var ldapConfig = config.ldap;
module.exports=function(usernameToTest,callBack){
	//var usernameToTest="testuser@vit.ac.in";
	var ad = new ActiveDirectory(ldapConfig);
	ad.userExists(usernameToTest, function(err, existsBoolean) {
		if(err){
			console.log(err);
			callBack(false);
		}
		else
			callBack(existsBoolean);
	});
}